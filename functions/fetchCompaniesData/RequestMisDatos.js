// const https = require('https');
// const request = require('request');
const fetch = require('node-fetch');
const { URLSearchParams } = require('url');

exports.handler = async (datos, context) => {
	// const data = JSON.stringify({
	// 	nit: 900926147
	// });

	var data = new URLSearchParams();
	data.append('nit', '900926147');

	fetch('https://api.misdatos.com.co/api/co/rues/consultarEmpresaPorNit', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			// 'Content-Length': data.length,
			'Authorization': '0yuh4m070sin6uauvmdzgidliivmghtk'
		},
		body: data
	})
		.then((response) => response.json())
		.then((result) => {
			console.log('RESULT', result);
		})
		.catch((error) => {
			console.log('ERROR', error);
		});

	// const options = {
	// 	hostname: 'api.misdatos.com.co/api/co/rues/consultarEmpresaPorNit',
	// 	// port: 80,
	// 	// path: '',
	// 	method: 'POST',

	// 	headers: {
	// 		'Content-Type': 'application/x-www-form-urlencoded',
	// 		'Content-Length': data.length,
	// 		'Authorization': '0yuh4m070sin6uauvmdzgidliivmghtk'
	// 	},
	// 	body: data
	// };

	// const req = https.request(options, (res) => {
	// 	console.log(`statusCode: ${res.statusCode}`);

	// 	res.on('data', (d) => {
	// 		console.log('data:', d);
	// 		process.stdout.write(d);
	// 	});
	// });

	// req.on('error', (error) => {
	// 	console.error(error);
	// });

	try {
		return 'SUCESS';
	} catch (error) {
		return 'ERROR';
	}
};
