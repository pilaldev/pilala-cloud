
exports.handler = (request, response, firestore, admin, cors) => {
    cors(request, response, () => {
        try {
            const userData = request.body.userData;
            const { seconds, nanoseconds } = request.body.dateRegistered;
            const dateRegistered = new admin.firestore.Timestamp(Number(seconds), Number(nanoseconds));
            const userEmail = request.body.userEmail.toLowerCase();
            const userRef = firestore.collection('users').doc(userEmail);

            userRef.set({
                ...userData,
                dateRegistered
            }, {merge: true}
            ).then(() => {
                return response.status(200).send({
                    status: 'SUCCESS',
                    message: 'Información actualizada correctamente'
                });

            }).catch((error) => {
                response.status(500).send({
                    status: 'FAILED',
                    message: 'Error actualizando el usuario nuevo',
                    error
                });
            });
        }
        catch (error) {
            response.status(500).send({
                status: 'FAILED',
                message:'Error ejecutando la cloud function de actualización de usuarios', 
                error
            })
        }
    });
}