/////////////////// IMPORT FIREBASE MODULES ///////////////////

const admin = require('firebase-admin');
admin.initializeApp();
const functions = require('firebase-functions');
const { Firestore } = require('@google-cloud/firestore');
const firestore = new Firestore();

////////////////////////////////////////////////////////////////

/////////////////// IMPORT ADITIONAL MODULES ///////////////////

const sgMail = require('@sendgrid/mail');

const cors = require('cors')({
	origin: true
});

////////////////////////////////////////////////////////////////

/////////////////// IMPORT FUNCTIONS ///////////////////

const SendInvitationMail = require('./invitationPanel/SendInvitationMail');
const RequestMisDatos = require('./fetchCompaniesData/RequestMisDatos');
const SendUserData = require('./SendUserData');
const GetResources = require('./GetResources');
const sendContactData = require('./companyDataInfo/sendContactData');
const sendManagerData = require('./companyDataInfo/sendManagerData');
const sendCompanyData = require('./companyDataInfo/sendCompanyData');

////////////////////////////////////////////////////////////////

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//


 exports.sendManagerData = functions.https.onRequest((request, response) => {
      sendManagerData.handler(request, response, firestore, admin, cors);
 });
 exports.sendCompanyData = functions.https.onRequest((request, response) => {
      sendCompanyData.handler(request, response, firestore, admin, cors);
 });
 exports.sendContactData = functions.https.onRequest((request, response) => {
      sendContactData.handler(request, response, firestore, admin, cors);
 });

 /* CF que escribe en firestore la información de un nuevo usuario que crea su cuenta */
 exports.sendUserData = functions.https.onRequest((request, response) => {
      SendUserData.handler(request, response, firestore, admin, cors);
 });

 /* CF para traer diferentes recursos de la app desde firestore */
 exports.getResources = functions.https.onRequest((request, response) => {
      GetResources.handler(request, response, firestore, cors);
 });
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.sendInvitationMail = functions.firestore.document('users/{user}').onUpdate((change, context) => {
	return SendInvitationMail.handler(change, context, sgMail, functions, firestore, admin);
});

exports.requestMisDatos = functions.https.onCall((data, context) => {
	return RequestMisDatos.handler(data, context);
});

