exports.handler = (request, response, firestore, cors) => {
    cors(request, response, () => {
        try {
            const locationRef = firestore.collection('recursos').doc('provinces_cities');
            const ciiuRef = firestore.collection('recursos').doc('ciiu');
            let data = {};

            locationRef.get().then((locationResult) => {
                const { cities, provinces } = locationResult.data();
                data = {
                    ...data,
                    cities,
                    provinces
                }
                ciiuRef.get().then((ciiuResult) => {
                    const { codeList } = ciiuResult.data();
                    data = {
                        ...data,
                        codeList
                    }
                    response.status(200).send({
                        status: 'SUCCESS',
                        message:'Departamentos, municipios y ciiu leídos correctamente', 
                        ...data
                    });
                }).catch((error) => {
                    response.status(500).send({
                        status: 'FAILED',
                        message:'Error leyendo el documento de ciiu', 
                        error
                    });
                });
            }).catch((error) => {
                response.status(500).send({
                    status: 'FAILED',
                    message:'Error leyendo el documento de departamentos y municipios', 
                    error
                });
            });
        }
        catch (error) {
            response.status(500).send({
                status: 'FAILED',
                message:'Error ejecutando la cloud function de obtener recursos', 
                error
            });
        }
    });
}