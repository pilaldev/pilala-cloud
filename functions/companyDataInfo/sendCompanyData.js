
exports.handler = (request, response, firestore, admin, cors) => {
    cors(request, response, () => {
        try {

            console.log("entre y  llego ",request)

            let body = request.body
            console.log("body",body)

            let location = body.location;
            let companyData = body.companyData;
            // let dateRegistered = admin.FieldValue.serverTimestamp();
            let adminPlatform = body.adminPlatform;
            let nitCompany = body.nitCompany;
            let provinceRef = firestore.collection('aportantes').doc(location.province.toLowerCase())
            let userRef = firestore.collection('users').doc(adminPlatform.email)
            let managerRef = firestore.collection('aportantes').doc(location.province.toLowerCase()).collection(location.city.toLowerCase()).doc(nitCompany);
            let cityRef = firestore.collection('aportantes').doc(location.province.toLowerCase()).collection(location.city.toLowerCase()).doc(nitCompany)
            provinceRef.get()
                .then(doc => {
                    if (!doc.exists) {
                        console.log('No such document!');
                    } else {
                        cityRef.get()
                            .then(doc => {
                                if (!doc.exists) {
                                    managerRef.set({
                                        id: nitCompany,
                                        location: { ...location },
                                        companyName: companyData.companyName,
                                        arl: companyData.arl,
                                        cajaCompensacion: companyData.cajaCompensacion,
                                        phoneNumber: companyData.phoneNumber,
                                        mobileNumber: companyData.mobileNumber,
                                        NIT: nitCompany,
                                        platformAdmin: {
                                            ...adminPlatform
                                        }
                                        //legalRepresentative: { ...companyData},
                                        //dateRegistered:dateRegistered
                                    }, { merge: true }
                                    ).then(() => {
                                        provinceRef.update({
                                            totalCompanies: admin.firestore.FieldValue.increment(1),
                                            // [`total${location.city}`]: firebase.firestore.FieldValue.increment(1),
                                        }).then(
                                            userRef.get().then(doc => {
                                                if (!doc.exists) {
                                                    response.status(500).send({
                                                        status: 'FAILED',
                                                        message: `Usuario ${adminPlatform.email}  no encontrado`
                                                    })
                                                }
                                                else {
                                                    userRef.update({
                                                        companies: admin.firestore.FieldValue.arrayUnion({
                                                            nitCompany: nitCompany,
                                                            companyData: companyData.companyName,
                                                            province: location.province,
                                                            city: location.city
                                                        }),
                                                        createdCompany:true
                                                    }).then(
                                                        () => {
                                                            return response.status(200).send({
                                                                status: 'SUCCESS',
                                                                message: 'Aportante creado correctamente'
                                                            })
                                                        })
                                                }
                                            })
                                        )
                                    }).catch((error) => {
                                        response.status(500).send({
                                            status: 'FAILED',
                                            message: 'Error creando el aportante nuevo',
                                            error
                                        });
                                    });
                                } else {
                                    managerRef.update({
                                        location: { ...location },
                                        companyName: companyData.companyName,
                                        arl: companyData.arl,
                                        cajaCompensacion: companyData.cajaCompensacion,
                                        phoneNumber: companyData.phoneNumber,
                                        mobileNumber: companyData.mobileNumber,
                                        platformAdmin: { ...adminPlatform }
                                        //dateRegistered:dateRegistered
                                    }).then(() => {
                                        return response.status(200).send({
                                            status: 'SUCCESS',
                                            message: 'Información actualizada correctamente'
                                        });

                                    }).catch((error) => {
                                        response.status(500).send({
                                            status: 'FAILED',
                                            message: 'Error actualizando el aportante nuevo',
                                            error
                                        });
                                    });
                                }
                            })
                            .catch(err => {
                                console.log('Error getting document', err);
                            });
                        console.log('Document data:', doc.data());
                    }
                })
                .catch(err => {
                    console.log('Error getting document', err);
                });

        }
        catch (error) {
            response.status(500).send({
                status: 'FAILED',
                message: 'Error ejecutando la cloud function de actualización de usuarios',
                error
            })
        }

    });
}