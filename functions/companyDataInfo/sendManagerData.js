
exports.handler = (request, response, firestore, admin, cors) => {
    cors(request, response, () => {
        try {
            const body = request.body
            const location = body.location;
            const managerData = body.managerData;
            //const dateRegistered = admin.FieldValue.serverTimestamp();
            const adminPlatform = body.adminPlatform;
            const nitCompany = body.nitCompany;
            let provinceRef = firestore.collection('aportantes').doc(location.province.toLowerCase())
            let userRef = firestore.collection('users').doc(adminPlatform.email)
            let managerRef = firestore.collection('aportantes').doc(location.province.toLowerCase()).collection(location.city.toLowerCase()).doc(nitCompany);
            let cityRef = firestore.collection('aportantes').doc(location.province.toLowerCase()).collection(location.city.toLowerCase()).doc(nitCompany)
            provinceRef.get()
                .then(doc => {
                    if (!doc.exists) {
                        console.log('No such document!');

                    } else {
                        cityRef.get()
                            .then(doc => {
                                if (!doc.exists) {
                                    cityRef.set({
                                        legalRepresentative: { ...managerData },
                                        //dateRegistered:dateRegistered
                                    }, { merge: true }
                                    ).then(
                                        () => {
                                            return response.status(200).send({
                                                status: 'SUCCESS',
                                                message: 'Aportante creado correctamente'
                                            })

                                        }).catch((error) => {
                                            response.status(500).send({
                                                status: 'FAILED',
                                                message: 'Error creando el aportante nuevo',
                                                error
                                            });
                                        });
                                } else  {
                                    cityRef.update({
                                        legalRepresentative:{...managerData}
                                        //dateRegistered:dateRegistered
                                    }).then(() => {
                                        return response.status(200).send({
                                            status: 'SUCCESS',
                                            message: 'Información actualizada correctamente'
                                        });

                                    }).catch((error) => {
                                        response.status(500).send({
                                            status: 'FAILED',
                                            message: 'Error actualizando el usuario nuevo',
                                            error
                                        });
                                    });
                                    console.log('Document data:', doc.data());
                                }
                            })
                            .catch(err => {
                                console.log('Error getting document', err);
                            });
                        console.log('Document data:', doc.data());
                    }
                })
                .catch(err => {
                    console.log('Error getting document', err);
                });

        }
        catch (error) {
            response.status(500).send({
                status: 'FAILED',
                message: 'Error ejecutando la cloud function de actualización de usuarios',
                error
            })
        }

    });
}